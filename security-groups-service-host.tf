# Security Groups and Rules

# Remark: Do not mix fixed IP Ranges and dynamically given ressources
# by other security groups. Create 2 security groups for that case
# with one part static / second part dynamically

### Service Host Security Group Rules for Service Host ###
resource "aws_security_group" "service-host" {
  count       = "${var.create_service_host ? 1 : 0}"
  name        = "service-host-${var.env}"
  description = "Allow service-host communication"
  vpc_id      = "${aws_vpc.prunux-mgt.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_service_host_communication"
  }
}

resource "aws_security_group_rule" "http-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "https-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "syslog-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 514
  to_port                  = 514
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "telegraf-telnet-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 4242
  to_port                  = 4242
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "telegraf-https-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 4443
  to_port                  = 4443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "journal-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 19532
  to_port                  = 19532
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "amqp-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 5671
  to_port                  = 5671
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "apt-proxy-from-vpc-hosts" {
  count                    = "${var.create_service_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 9999
  to_port                  = 9999
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.service-host.id}"
  source_security_group_id = "${aws_security_group.prunux_mgt.id}"
}

## default DB group from service host

resource "aws_security_group_rule" "db_default_allow_mysql-from-sh" {
  count = "${var.create_service_host ? 1 : 0}"

  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.db_default.id}"
  source_security_group_id = "${aws_security_group.service-host.id}"
}

resource "aws_security_group_rule" "db_default_allow_postgresql-from-sh" {
  count = "${var.create_service_host ? 1 : 0}"

  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.db_default.id}"
  source_security_group_id = "${aws_security_group.service-host.id}"
}
