# Security Groups and Rules

# Remark: Do not mix fixed IP Ranges and dynamically given ressources
# by other security groups. Create 2 security groups for that case
# with one part static / second part dynamically

### Bastion Host Security Group Rules for Bastion Host ###
resource "aws_security_group" "bastion-host" {
  count       = "${var.create_bastion_host ? 1 : 0}"
  name        = "bastion-host-${var.env}"
  description = "Allow bastion-host communication"
  vpc_id      = "${aws_vpc.prunux-mgt.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    ipv6_cidr_blocks = ["::/0"]
  }

  tags {
    Name = "allow_bastion_host_communication"
  }
}

resource "aws_security_group_rule" "ssh-from-bastion-host" {
  count                    = "${var.create_bastion_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.env_default.id}"
  source_security_group_id = "${aws_security_group.bastion-host.id}"
}

resource "aws_security_group_rule" "http-from-bastion-host" {
  count                    = "${var.create_bastion_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.env_default.id}"
  source_security_group_id = "${aws_security_group.bastion-host.id}"
}

resource "aws_security_group_rule" "https-from-bastion-host" {
  count                    = "${var.create_bastion_host ? 1 : 0}"
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.env_default.id}"
  source_security_group_id = "${aws_security_group.bastion-host.id}"
}
