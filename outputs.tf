# the id of this vpc
output "vpc_id" {
  value = "${aws_vpc.prunux-mgt.id}"
}

output "hosted_zone_id" {
  description = "hosted zone id of the vpc route53 private zone"
  value       = "${aws_route53_zone.vpc_prunux_net.zone_id}"
}

# the subnet ids (count should equal the az count)
output "subnet_prunux_mgt_public_ids" {
  value = ["${aws_subnet.prunux-mgt-public.*.id}"]
}

output "route_table_id" {
  value = "${aws_route_table.prunux-mgt-public-rt.id}"
}

# output the profile name so this can be used within any ec2 instance
# output "instance_profile_name" {
#   value = "${aws_iam_instance_profile.common.name}"
# }


# output assume_role_id so other policies can attach to this
# output "assume_role_id" {
#   value = "${aws_iam_role.ec2_assume_role.id}"
# }


# output the service-host assume_role_id so other policies can attach to this
# output "assume_role_sh_id" {
#   value = "${aws_iam_role.ec2_assume_role_sh.id}"
# }


# security group ids
# output "security_group_default_id" {
#   value = "${aws_default_security_group.default.id}"
# }


# output "security_group_prunux_mgt_id" {
#   value = "${aws_security_group.prunux_mgt.id}"
# }


# output "security_group_bastion-host_id" {
#   value = "${aws_security_group.bastion-host.id}"
# }


# output "security_group_env_default_id" {
#   value = "${aws_security_group.env_default.id}"
# }


# output "security_group_db_default_id" {
#   value = "${aws_security_group.db_default.id}"
# }


# output "sns_topic_workhours" {
#   value = "${aws_sns_topic.cloudwatch_alerts_workhours.arn}"
# }


# output "sns_topic_offhours" {
#   value = "${aws_sns_topic.cloudwatch_alerts_offhours.arn}"
# }

