# Security Groups and Rules

# Remark: Do not mix fixed IP Ranges and dynamically given ressources
# by other security groups. Create 2 security groups for that case
# with one part static / second part dynamically

### Default Security Group Rules for env_default ###
resource "aws_security_group" "env_default" {
  name        = "env-default-${var.env}"
  description = "Prunux Managed EC2 Default"
  vpc_id      = "${aws_vpc.prunux-mgt.id}"

  tags {
    Name = "prunux-mgt-ec2-default"
  }
}

resource "aws_security_group_rule" "env_default_allow_internal" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  security_group_id = "${aws_security_group.env_default.id}"
  self              = true
}

resource "aws_security_group_rule" "env_default_allow_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.env_default.id}"
}

resource "aws_security_group_rule" "env_default_allow_smtp" {
  type              = "ingress"
  from_port         = 25
  to_port           = 25
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.env_default.id}"
}

resource "aws_security_group_rule" "env_default_allow_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.env_default.id}"
}

resource "aws_security_group_rule" "env_default_allow_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.env_default.id}"
}

### Default Security Group Rules for db-default ###
resource "aws_security_group" "db_default" {
  name        = "db-default-${var.env}"
  description = "Prunux Managed DB Default"
  vpc_id      = "${aws_vpc.prunux-mgt.id}"

  tags {
    Name = "prunux-mgt-db-default"
  }
}

resource "aws_security_group_rule" "db_default_allow_mysql" {
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.db_default.id}"
  source_security_group_id = "${aws_security_group.env_default.id}"
}

resource "aws_security_group_rule" "db_default_allow_postgresql" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.db_default.id}"
  source_security_group_id = "${aws_security_group.env_default.id}"
}
