variable "env" {}
variable "aws_region" {}
variable "customer_name" {}
variable "customer_name_aws" {}
variable "customer_id" {}
variable "prunux_cust_cidr" {}

variable "aws_key_name" {
  default = "prunux-mgmt@aws"
}
variable "customer_domain_name" {
  default = "prunux.net"
}

# sets the newbits for the cidr_block when creating the subnets
variable "cidr_newbits" {
  default = 8
}

# sets the newbits for the cidr_block when creating the subnets
variable "cidr_newbits_ipv6" {
  default = 16
}

variable "map_public_ip_on_subnet" {
  default = true
}

variable "create_bastion_host" {
  default = false
}

variable "bastion_host_disk_size" {
  description = "default disk size of bastion host"
  default     = 10
}

variable "create_service_host" {
  default = false
}

variable "service_host_disk_size" {
  description = "default disk size of service host"
  default     = 10
}

variable "remote_exec_user" {
  description = "The SSH user which will be used to perform remote-exec"
  default     = "admin"
}

variable "enable_ebs_backup" {
  default = true
}

variable "ebs_backup_retention" {
  description = "Retention of all EBS snapshots in days, defaults to 30"
  default     = 30
}
