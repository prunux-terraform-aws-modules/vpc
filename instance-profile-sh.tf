# instance profile for service-host
resource "aws_iam_instance_profile" "service-host" {
  count = "${var.create_service_host ? 1 : 0}"
  name  = "instance-profile-${var.aws_region}-${module.common.vpcs[var.env]}-sh"
  role  = "${aws_iam_role.ec2_assume_role_sh.name}"

  # the instance_profile is not created instantly, so we have to wait
  # this should be fixed according to https://github.com/hashicorp/terraform/issues/9474
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

# role to be used and assumed for service-host
resource "aws_iam_role" "ec2_assume_role_sh" {
  count = "${var.create_service_host ? 1 : 0}"
  name  = "ec2-assume-${var.aws_region}-${module.common.vpcs[var.env]}-sh"

  assume_role_policy = <<EOF
{
  "Statement" : [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ],
  "Version" : "2012-10-17"
}
EOF
}

# attachments 
## service host
resource "aws_iam_role_policy_attachment" "read_tags_attach_sh" {
  count      = "${var.create_service_host ? 1 : 0}"
  role       = "${aws_iam_role.ec2_assume_role_sh.id}"
  policy_arn = "${aws_iam_policy.read_tags_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "read_cloudwatch_attach_sh" {
  count      = "${var.create_service_host ? 1 : 0}"
  role       = "${aws_iam_role.ec2_assume_role_sh.id}"
  policy_arn = "${aws_iam_policy.read_cloudwatch_policy.arn}"
}
