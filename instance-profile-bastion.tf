# instance profile for bastion-host
resource "aws_iam_instance_profile" "bastion-host" {
  count = "${var.create_bastion_host ? 1 : 0}"

  name = "instance-profile-${var.aws_region}-${module.common.vpcs[var.env]}-bastion"
  role = "${aws_iam_role.ec2_assume_role_bastion.name}"

  # the instance_profile is not created instantly, so we have to wait
  # this should be fixed according to https://github.com/hashicorp/terraform/issues/9474
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

# role to be used and assumed for bastion-host
resource "aws_iam_role" "ec2_assume_role_bastion" {
  count = "${var.create_bastion_host ? 1 : 0}"

  name = "ec2-assume-${var.aws_region}-${module.common.vpcs[var.env]}-bastion"

  assume_role_policy = <<EOF
{
  "Statement" : [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ],
  "Version" : "2012-10-17"
}
EOF
}

## bastion host
resource "aws_iam_role_policy_attachment" "read_tags_attach_bastion" {
  count      = "${var.create_bastion_host ? 1 : 0}"
  role       = "${aws_iam_role.ec2_assume_role_bastion.id}"
  policy_arn = "${aws_iam_policy.read_tags_policy.arn}"
}
