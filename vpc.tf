# Create a VPC
resource "aws_vpc" "prunux-mgt" {
  cidr_block                       = "${var.prunux_cust_cidr}"
  assign_generated_ipv6_cidr_block = true
  enable_dns_support               = true
  enable_dns_hostnames             = true

  tags {
    Name        = "prunux-mgt-${var.env} VPC"
    Environment = "${var.env}"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "prunux-mgt-gw" {
  vpc_id = "${aws_vpc.prunux-mgt.id}"

  tags {
    Name        = "prunux-mgt-${var.env} GW"
    Environment = "${var.env}"
  }
}

# Public Subnets
resource "aws_subnet" "prunux-mgt-public" {
  count = "${length(module.common.availability_zones)}"

  cidr_block              = "${cidrsubnet(aws_vpc.prunux-mgt.cidr_block, var.cidr_newbits, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  ipv6_cidr_block                 = "${cidrsubnet(aws_vpc.prunux-mgt.ipv6_cidr_block, var.cidr_newbits, count.index + 1)}"
  assign_ipv6_address_on_creation = "${var.map_public_ip_on_launch}"

  availability_zone = "${module.common.availability_zones[count.index]}"
  vpc_id            = "${aws_vpc.prunux-mgt.id}"

  tags {
    Name        = "prunux-mgt-${var.env}-${module.common.availability_zones[count.index]}-public"
    Environment = "${var.env}"
  }
}

resource "aws_route_table" "prunux-mgt-public-rt" {
  vpc_id = "${aws_vpc.prunux-mgt.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.prunux-mgt-gw.id}"
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = "${aws_internet_gateway.prunux-mgt-gw.id}"
  }

  tags {
    Name        = "prunux-mgt-${var.env}"
    Environment = "${var.env}"
  }
}

resource "aws_route_table_association" "prunux-mgt-public-rt-association" {
  count          = "${length(module.common.availability_zones)}"
  subnet_id      = "${element(aws_subnet.prunux-mgt-public.*.id, count.index)}"
  route_table_id = "${aws_route_table.prunux-mgt-public-rt.id}"
}

# replace main route table
resource "aws_main_route_table_association" "main" {
  vpc_id         = "${aws_vpc.prunux-mgt.id}"
  route_table_id = "${aws_route_table.prunux-mgt-public-rt.id}"
}

# Default Security Group for VPC
resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.prunux-mgt.id}"

  ingress {
    self      = true
    from_port = 0
    to_port   = 0
    protocol  = "-1"
  }

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    ipv6_cidr_blocks = ["::/0"]
  }

  tags {
    Name = "default"
  }
}

resource "aws_vpc_endpoint" "prunux-mgt-s3-access" {
  vpc_id       = "${aws_vpc.prunux-mgt.id}"
  service_name = "com.amazonaws.${var.aws_region}.s3"
}

# Private Link DynamoDB Service to this VPC
resource "aws_vpc_endpoint" "prunux-mgt-dynamodb-access" {
  vpc_id       = "${aws_vpc.prunux-mgt.id}"
  service_name = "com.amazonaws.${var.aws_region}.dynamodb"
}
