# Security Groups and Rules

# Remark: Do not mix fixed IP Ranges and dynamically given ressources
# by other security groups. Create 2 security groups for that case
# with one part static / second part dynamically

### Default Security Group Rules for prunux-mgt ###
resource "aws_security_group" "prunux_mgt" {
  name        = "prunux-mgt"
  description = "Prunux Managed Environements in AWS"
  vpc_id      = "${aws_vpc.prunux-mgt.id}"

  tags {
    Name = "prunux-mgt-${var.env}"
  }
}

resource "aws_security_group_rule" "prunux_mgt_allow_ssh-from-pb" {
  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = [
    "${module.common-private.prunux_networks_ipv4}",
  ]

  ipv6_cidr_blocks = [
    "${module.common-private.prunux_networks_ipv6}",
  ]

  description = "Allow SSH Access from prunux managment servers"

  security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "prunux_mgt_allow_icmp_ipv4" {
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "prunux_mgt_allow_outgoing_ipv4" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.prunux_mgt.id}"
}

resource "aws_security_group_rule" "prunux_mgt_allow_outgoing_ipv6" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = "${aws_security_group.prunux_mgt.id}"
}
